import pytest
from tkinter import *
import main

calc = main.Main(Tk())


def test_zero_division():
    with pytest.raises(ZeroDivisionError):
        calc.formula = "10/0"
        calc.logicalc("=")


@pytest.mark.parametrize("formula, right_result", [("5+5", "10"),
                                                   ("100-20", "80"),
                                                   ("5*5", "25"),
                                                   ("30/6", "5.0")])
def test_base(formula, right_result):
    calc.formula = formula
    calc.logicalc("=")
    assert calc.formula == right_result


@pytest.mark.parametrize("formula, operator, right_result", [("1000", "C", "0"),
                                                             ("666", "DEL", "66"),
                                                             ("5", "X^2", "25")
                                                             ])
def test_hard(formula, operator, right_result):
    calc.formula = formula
    calc.logicalc(operator)
    assert calc.formula == right_result
